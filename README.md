# Users API RESTful + login

Web API developed using .Net Core, Entity Framerwok and JWT fot authentication. The main goal of this API is to create a user, login and get logged user informations.

## How to execute

Clone this project into some folder of your choice and run the following command:

    dotnet run --project .\src\UsersApiLogin.WebApi\

## Routes
<br>

### POST -  /api/user/signup
This route expects an user as described bellow

```json
    {
        "firstName": "Hello",
        "lastName": "World",
        "email": "hello@world.com",
        "password": "hunter2",
        "phones": [
            {
                "number": "988887888",
                "area_code": "81",
                "country_code": "+55"
            }
        ]
    }
```
<br>

### POST - /api/auth/signin
This route expects e-mail and password to signin an user
```json
{
    "email": "hello@world.com",
    "password": "hunter2"
}
```
It will return the JWT token used to login, besides some user information

<br>

### GET- /api/user/me
This route waits for the user's token to return user information. Token should be passed through Authorization Header.

- Authorization - Bearer yourJwtToken
