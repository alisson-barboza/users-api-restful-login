﻿using System;

namespace UsersApiLogin.Domain.Entities
{
    public class Phone
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public string AreaCode { get; set; }
        public string CountryCode { get; set; }
    }
}
