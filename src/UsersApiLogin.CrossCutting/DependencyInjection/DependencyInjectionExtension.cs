﻿using Microsoft.Extensions.DependencyInjection;
using UsersApiLogin.Application.IService;
using UsersApiLogin.Application.Service;
using UsersApiLogin.Application.Service.Security.Jwt;
using UsersApiLogin.Domain.IService;
using UsersApiLogin.Domain.Service;
using UsersApiLogin.Infrastructure.IRepository;
using UsersApiLogin.Infrastructure.Repository;

namespace UsersApiLogin.CrossCutting.DependencyInjection
{
    public static class DependencyInjectionExtension
    {
        public static void Inject(this IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<ITokenGenerator, JwtGenerator>();

            services.AddScoped(typeof(IBaseDomainService<>), typeof(BaseDomainService<>));
            services.AddTransient<IUserDomainService, UserDomainService>();

            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUserRepository, UserRepository>();
        }
    }
}
