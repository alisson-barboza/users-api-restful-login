﻿using AutoMapper;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Application.Service.Security;
using UsersApiLogin.CrossCutting.AutoMapper.Formatter;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.CrossCutting.AutoMapper
{
    public class UserSignUpMapper : Profile
    {
        public UserSignUpMapper()
        {
            CreateMap<SignUpDTO, User>()
                .ForMember(p => p.Id, opt => opt.Ignore())
                .ForMember(p => p.CreationDate, opt => opt.Ignore())
                .ForMember(p => p.LastLogin, opt => opt.Ignore())
                .ForMember(p => p.Phones, opt => opt.Ignore())
                .ForMember(p => p.Password, opt => opt.MapFrom(p => p.Password.HashPassword()));

            CreateMap<User, SignUpDTO>()
                .ForMember(p => p.Phones, opt => opt.ConvertUsing(new PhonesMapperFormatter(), p => p.Phones));
        }
    }
}
