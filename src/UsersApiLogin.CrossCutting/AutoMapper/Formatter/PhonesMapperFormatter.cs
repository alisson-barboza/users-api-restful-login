﻿using AutoMapper;
using System.Collections.Generic;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.CrossCutting.AutoMapper.Formatter
{
    public class PhonesMapperFormatter : IValueConverter<ICollection<Phone>, ICollection<PhoneDTO>>
    {
        public ICollection<PhoneDTO> Convert(ICollection<Phone> sourceMember, ResolutionContext context)
        {
            ICollection<PhoneDTO> phones = new List<PhoneDTO>();
            if (sourceMember == null)
            {
                return phones;
            }
            foreach (var phone in sourceMember)
            {
                phones.Add(new PhoneDTO
                {
                    Area_Code = phone.AreaCode,
                    Country_Code = phone.CountryCode,
                    Number = phone.Number
                });
            }
            return phones;
        }
    }
}
