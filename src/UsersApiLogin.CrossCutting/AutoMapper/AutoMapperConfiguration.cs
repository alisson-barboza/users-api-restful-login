﻿using AutoMapper;

namespace UsersApiLogin.CrossCutting.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static MapperConfiguration ConfigureMappers()
        {
            return new MapperConfiguration(m =>
            {
                m.AddProfile(new UserSignUpMapper());
                m.AddProfile(new UserUserInfoMapper());
            });
        }
    }
}
