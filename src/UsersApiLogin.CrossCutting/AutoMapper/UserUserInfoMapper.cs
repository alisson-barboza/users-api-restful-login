﻿using AutoMapper;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.CrossCutting.AutoMapper.Formatter;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.CrossCutting.AutoMapper
{
    class UserUserInfoMapper : Profile
    {
        public UserUserInfoMapper()
        {
            CreateMap<UserInfoDTO, User>()
                .ForMember(p => p.Id, opt => opt.Ignore())
                .ForMember(p => p.Password, opt => opt.Ignore())
                .ForMember(p => p.Phones, opt => opt.Ignore());

            CreateMap<User, UserInfoDTO>()
                .ForMember(p => p.Phones, opt => opt.ConvertUsing(new PhonesMapperFormatter(), p => p.Phones));

        }

    }
}
