﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Infrastructure.IRepository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task UpdateLastLogin(User user);
        Task<User> FindUser(Expression<Func<User, bool>> expression);
    }

}
