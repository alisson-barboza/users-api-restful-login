﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Infrastructure.IRepository
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<T> Save(T entity);
        Task<T> Find(Expression<Func<T, bool>> expression);
    }
}
