﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Infrastructure.EntityFramework.DatabaseContext;
using UsersApiLogin.Infrastructure.IRepository;

namespace UsersApiLogin.Infrastructure.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected DbSet<T> entities;
        protected ApplicationContext applicationContext;

        public BaseRepository(ApplicationContext applicationContext)
        {
            this.entities = applicationContext.Set<T>();
            this.applicationContext = applicationContext;
        }

        public async Task<T> Find(Expression<Func<T, bool>> expression)
        {
            return await entities.Where(expression).FirstOrDefaultAsync();
        }

        public async Task<T> Save(T entity)
        {
            entity.CreationDate = DateTime.Now;
            var savedEntity = entities.Add(entity).Entity;
            await applicationContext.SaveChangesAsync();
            return savedEntity;
        }
    }
}
