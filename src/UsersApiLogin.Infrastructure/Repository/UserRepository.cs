﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Infrastructure.EntityFramework.DatabaseContext;
using UsersApiLogin.Infrastructure.IRepository;

namespace UsersApiLogin.Infrastructure.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationContext applicationContext) : base(applicationContext)
        {
        }

        public async Task<User> FindUser(Expression<Func<User, bool>> expression)
        {
            return await entities.Where(expression).Include(u => u.Phones).FirstOrDefaultAsync();
        }

        public async Task UpdateLastLogin(User user)
        {
            applicationContext.Attach(user);
            applicationContext.Entry(user).State = EntityState.Modified;
            await applicationContext.SaveChangesAsync();
        }
    }
}
