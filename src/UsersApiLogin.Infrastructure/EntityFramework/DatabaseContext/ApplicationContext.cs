﻿using Microsoft.EntityFrameworkCore;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Infrastructure.EntityFramework.EntityMapper;

namespace UsersApiLogin.Infrastructure.EntityFramework.DatabaseContext
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserEntityMapper());
            modelBuilder.ApplyConfiguration(new PhoneEntityMapper());
        }
    }
}
