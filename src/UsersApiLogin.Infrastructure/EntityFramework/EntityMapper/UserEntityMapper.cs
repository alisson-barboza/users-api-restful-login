﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Infrastructure.EntityFramework.EntityMapper
{
    public class UserEntityMapper : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.HasIndex(u => u.Email).IsUnique();
            builder.Property(u => u.CreationDate).IsRequired().ValueGeneratedOnAdd();

            builder.Property(u => u.LastName).IsRequired();
            builder.Property(u => u.FirstName).IsRequired();
            builder.Property(u => u.Password).IsRequired();

            builder.HasMany(u => u.Phones).WithOne().OnDelete(DeleteBehavior.Cascade);

        }
    }
}
