﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Infrastructure.EntityFramework.EntityMapper
{
    public class PhoneEntityMapper : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.HasKey(u => u.Id);
        }
    }
}
