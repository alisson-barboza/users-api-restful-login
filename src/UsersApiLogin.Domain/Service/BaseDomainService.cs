﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Domain.IService;
using UsersApiLogin.Infrastructure.IRepository;
namespace UsersApiLogin.Domain.Service
{
    public class BaseDomainService<T> : IBaseDomainService<T> where T : BaseEntity
    {
        private readonly IBaseRepository<T> baseRepository;

        public BaseDomainService(IBaseRepository<T> baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public async Task<T> Find(Expression<Func<T, bool>> expression)
        {
            return await baseRepository.Find(expression);
        }

        public Task<T> Save(T entity)
        {
            return baseRepository.Save(entity);
        }
    }
}
