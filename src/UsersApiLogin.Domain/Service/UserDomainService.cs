﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Domain.IService;
using UsersApiLogin.Infrastructure.IRepository;

namespace UsersApiLogin.Domain.Service
{
    public class UserDomainService : BaseDomainService<User>, IUserDomainService
    {
        private readonly IUserRepository userRepository;
        public UserDomainService(IBaseRepository<User> baseRepository, IUserRepository userRepository) : base(baseRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<User> FindUser(Expression<Func<User, bool>> expression)
        {
            return await userRepository.FindUser(expression);
        }

        public async Task UpdateLastLogin(User user)
        {
            user.LastLogin = DateTime.Now;
            await userRepository.UpdateLastLogin(user);
        }
    }
}
