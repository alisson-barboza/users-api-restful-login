﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Domain.IService
{
    public interface IUserDomainService : IBaseDomainService<User>
    {
        Task UpdateLastLogin(User user);
        Task<User> FindUser(Expression<Func<User, bool>> expression);
    }
}
