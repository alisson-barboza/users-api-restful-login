﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Domain.IService
{
    public interface IBaseDomainService<T> where T : BaseEntity
    {
        Task<T> Find(Expression<Func<T, bool>> expression);
        Task<T> Save(T entity);
    }
}
