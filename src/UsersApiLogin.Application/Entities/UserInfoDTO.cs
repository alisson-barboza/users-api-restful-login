﻿using System.Collections.Generic;

namespace UsersApiLogin.Application.Entities
{
    public class UserInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public ICollection<PhoneDTO> Phones { get; set; }
        public string CreationDate { get; set; }
        public string LastLogin { get; set; }
    }
}
