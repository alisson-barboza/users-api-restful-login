﻿namespace UsersApiLogin.Application.Entities
{
    public class SignedInDTO
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Token { get; set; }
    }
}
