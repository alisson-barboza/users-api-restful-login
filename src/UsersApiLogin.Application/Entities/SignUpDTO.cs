﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UsersApiLogin.Application.Entities
{
    public class SignUpDTO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [RegularExpression(@"^[a-zA-Z0-9\u00C0-\u00FF ]+$", ErrorMessage = "Invalid fields")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [RegularExpression(@"^[a-zA-Z0-9\u00C0-\u00FF ]+$", ErrorMessage = "Invalid fields")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [EmailAddress(ErrorMessage = "Invalid fields")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Missing fields")]
        public ICollection<PhoneDTO> Phones { get; set; }
    }
}
