﻿using System.ComponentModel.DataAnnotations;

namespace UsersApiLogin.Application.Entities
{
    public class PhoneDTO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [RegularExpression(@"^\d{9}$", ErrorMessage = "Invalid fields")]
        public string Number { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [RegularExpression(@"^\d{2}$", ErrorMessage = "Invalid fields")]
        public string Area_Code { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [RegularExpression(@"^(\+\d{1,3}|\d{1,4})$", ErrorMessage = "Invalid fields")]
        public string Country_Code { get; set; }
    }
}
