﻿namespace UsersApiLogin.Application.Entities
{
    public class ResponseDTO
    {
        public string Message { get; set; }
        public string ErrorCode { get; set; }
    }
}
