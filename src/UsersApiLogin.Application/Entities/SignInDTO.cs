﻿using System.ComponentModel.DataAnnotations;

namespace UsersApiLogin.Application.Entities
{
    public class SignInDTO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        [EmailAddress(ErrorMessage = "Invalid fields")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Missing fields")]
        public string Password { get; set; }
    }
}
