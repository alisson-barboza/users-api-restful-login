﻿using UsersApiLogin.Domain.Entities;

namespace UsersApiLogin.Application.IService
{
    public interface ITokenGenerator
    {
        string GenerateToken(User user);
    }
}
