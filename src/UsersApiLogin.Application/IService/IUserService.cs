﻿using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;

namespace UsersApiLogin.Application.IService
{
    public interface IUserService
    {
        Task<SignUpDTO> AddUser(SignUpDTO userDTO);
        Task<UserInfoDTO> GetUserInformations(string email);
    }
}
