﻿using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;

namespace UsersApiLogin.Application.IService
{
    public interface IAuthService
    {
        Task<SignedInDTO> Login(SignInDTO signInDTO);
    }
}
