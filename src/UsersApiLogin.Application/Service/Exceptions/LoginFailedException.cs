﻿using System;

namespace UsersApiLogin.Application.Service.Exceptions
{
    public class LoginFailedException : ArgumentException
    {
        public LoginFailedException(string message) : base(message)
        {
        }
    }
}
