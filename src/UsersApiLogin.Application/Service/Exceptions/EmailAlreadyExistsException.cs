﻿using System;

namespace UsersApiLogin.Application.Service.Exceptions
{
    public class EmailAlreadyExistsException : ArgumentException
    {
        public EmailAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
