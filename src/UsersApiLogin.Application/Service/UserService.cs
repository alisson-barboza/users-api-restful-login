﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Application.IService;
using UsersApiLogin.Application.Service.Exceptions;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Domain.IService;

namespace UsersApiLogin.Application.Service
{
    public class UserService : IUserService
    {
        private readonly IUserDomainService userDomainService;
        private readonly IMapper mapper;

        public UserService(IMapper mapper, IUserDomainService userDomainService)
        {
            this.mapper = mapper;
            this.userDomainService = userDomainService;
        }

        public async Task<SignUpDTO> AddUser(SignUpDTO userDTO)
        {
            await CheckUserDuplicity(userDTO);
            var user = mapper.Map<User>(userDTO);
            user.Phones = CreatePhoneList(userDTO.Phones);
            return mapper.Map<SignUpDTO>(await userDomainService.Save(user));
        }

        private ICollection<Phone> CreatePhoneList(ICollection<PhoneDTO> phonesDTO)
        {
            ICollection<Phone> phones = new List<Phone>();
            foreach (var phone in phonesDTO)
            {
                phones.Add(new Phone { AreaCode = phone.Area_Code, CountryCode = phone.Country_Code, Number = phone.Number });
            }
            return phones;
        }

        private async Task CheckUserDuplicity(SignUpDTO userDTO)
        {
            var user = await userDomainService.Find(u => u.Email == userDTO.Email);
            if (user != null)
            {
                throw new EmailAlreadyExistsException("E-mail already exists");
            }
        }

        public async Task<UserInfoDTO> GetUserInformations(string email)
        {
            var user = await userDomainService.FindUser(u => u.Email == email);
            var userInfoDTO = mapper.Map<UserInfoDTO>(user);
            userInfoDTO.Phones = CreatePhoneDTOList(user.Phones);
            return userInfoDTO;
        }

        private ICollection<PhoneDTO> CreatePhoneDTOList(ICollection<Phone> phones)
        {
            ICollection<PhoneDTO> phonesDTO = new List<PhoneDTO>();
            foreach (var phone in phones)
            {
                phonesDTO.Add(new PhoneDTO { Area_Code = phone.AreaCode, Country_Code = phone.CountryCode, Number = phone.Number });
            }
            return phonesDTO;
        }
    }
}
