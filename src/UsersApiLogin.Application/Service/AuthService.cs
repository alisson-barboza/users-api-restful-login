﻿using AutoMapper;
using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Application.IService;
using UsersApiLogin.Application.Service.Exceptions;
using UsersApiLogin.Application.Service.Security;
using UsersApiLogin.Domain.Entities;
using UsersApiLogin.Domain.IService;

namespace UsersApiLogin.Application.Service
{
    public class AuthService : IAuthService
    {
        private readonly IMapper mapper;
        private readonly IUserDomainService userDomainService;
        private readonly ITokenGenerator tokenGenerator;

        public AuthService(IMapper mapper, IUserDomainService userDomainService, ITokenGenerator tokenGenerator)
        {
            this.mapper = mapper;
            this.userDomainService = userDomainService;
            this.tokenGenerator = tokenGenerator;
        }

        public async Task<SignedInDTO> Login(SignInDTO signInDTO)
        {
            User user = await GetUserWithValidCredentials(signInDTO);
            await userDomainService.UpdateLastLogin(user);
            return new SignedInDTO
            {
                Email = user.Email,
                FirstName = user.FirstName,
                Token = tokenGenerator.GenerateToken(user)
            };
        }

        private async Task<User> GetUserWithValidCredentials(SignInDTO signInDTO)
        {
            var user = await userDomainService.Find(u => u.Email == signInDTO.Email);
            if (user == null || !signInDTO.Password.HashPassword().Equals(user.Password))
            {
                throw new LoginFailedException("Invalid e-mail or password");
            }
            return user;
        }
    }
}
