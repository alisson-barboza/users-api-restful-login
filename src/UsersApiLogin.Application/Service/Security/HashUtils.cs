﻿using System.Security.Cryptography;
using System.Text;

namespace UsersApiLogin.Application.Service.Security
{
    public static class HashUtils
    {
        public static string HashPassword(this string raw)
        {
            using SHA256 Algorithm = SHA256.Create();
            byte[] bytes = Algorithm.ComputeHash(Encoding.UTF8.GetBytes(raw));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
