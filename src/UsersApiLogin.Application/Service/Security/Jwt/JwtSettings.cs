﻿namespace UsersApiLogin.Application.Service.Security.Jwt
{
    public class JwtSettings
    {
        public string Issuer { get; set; }
        public string Secret { get; set; }
        public int ExpirationInDays { get; set; }
    }
}
