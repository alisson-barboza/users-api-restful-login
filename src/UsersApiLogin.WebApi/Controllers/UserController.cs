﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Application.IService;

namespace UsersApiLogin.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }
        [HttpPost]
        [Route("signup")]
        public async Task<IActionResult> AddUser(SignUpDTO signUpDTO)
        {
            try
            {
                return Created(string.Empty, await userService.AddUser(signUpDTO));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ResponseDTO { Message = ex.Message, ErrorCode = "400" });
            }
        }

        [HttpGet]
        [Route("me")]
        [Authorize]
        public async Task<IActionResult> GetUserInfo()
        {
            var userEmail = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;

            return Ok(await userService.GetUserInformations(userEmail));
        }
    }
}
