﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UsersApiLogin.Application.Entities;
using UsersApiLogin.Application.IService;

namespace UsersApiLogin.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;
        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }
        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> SignIn(SignInDTO signInDTO)
        {
            try
            {
                return Ok(await authService.Login(signInDTO));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ResponseDTO { ErrorCode = "400", Message = ex.Message });
            }
        }
    }
}
