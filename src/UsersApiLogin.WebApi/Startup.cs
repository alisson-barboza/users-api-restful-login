using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UsersApiLogin.Application.Service.Security.Jwt;
using UsersApiLogin.CrossCutting.AutoMapper;
using UsersApiLogin.CrossCutting.DependencyInjection;
using UsersApiLogin.CrossCutting.Extension;
using UsersApiLogin.Infrastructure.EntityFramework.DatabaseContext;

namespace UsersApiLogin.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JwtSettings>(Configuration.GetSection("Jwt"));
            services.AddControllers();
            services.AddDbContext<ApplicationContext>(opt => opt.UseInMemoryDatabase("DB"));
            services.Inject();
            var mapconfig = AutoMapperConfiguration.ConfigureMappers();
            mapconfig.AssertConfigurationIsValid();
            IMapper mapper = mapconfig.CreateMapper();
            services.AddSingleton(mapper);
            var jwtSettings = Configuration.GetSection("Jwt").Get<JwtSettings>();
            services.AddAuth(jwtSettings);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuth();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
